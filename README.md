#Vue-widget
自己用的命令行小工具，自动创建模版文件

#option
- vw -e  // 在当前目录下创建新目录，目录内包括.es .html .less三个空文件
- vw -s, //创建简单标准文件夹,目录内包括.es .html .less三个文件,且.es文件已填充部分内容
- vw -f  // 在当前目录下创建搜索列表页模板文件夹， 目录内包括 filter list 等功能
- vw -o  // 打开模板目录, 可以修改本地模板，从而个性化
- vw -c  // 将分离的 Vue 组件合并成单文件 .vue [使用说明](http://git.oschina.net/g8up/Vue-widget/wikis/Vue-%E7%BB%84%E4%BB%B6%E5%90%88%E5%B9%B6%E5%8D%95%E6%96%87%E4%BB%B6(.vue))
- vw -h  // 帮助

# install
`npm install -g git+https://git.oschina.net/g8up/Vue-widget.git`

# 参考使用
- https://www.npmjs.com/package/yargs